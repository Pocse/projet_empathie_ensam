// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/WrinklesMapsNeck"
{
	Properties
	{
		_cou_AU27_527("cou_AU27_5-27", Range( 0 , 2)) = 1
		_cou_AU21_2021("cou_AU21_20-21", Range( 0 , 2)) = 1
		_Albedo("Albedo", 2D) = "white" {}
		_Nomalmapbase("Nomalmap base", 2D) = "bump" {}
		_metallicSmoothness("metallicSmoothness", 2D) = "white" {}
		_SmoothnessIntensity("Smoothness Intensity", Range( 0 , 1)) = 1
		_AO("AO", 2D) = "white" {}
		_AOIntensity("AO Intensity", Range( -1 , 0)) = 0
		_TextureArray0("Texture Array 0", 2DArray ) = "" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.5
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Nomalmapbase;
		uniform float4 _Nomalmapbase_ST;
		uniform UNITY_DECLARE_TEX2DARRAY( _TextureArray0 );
		uniform float4 _TextureArray0_ST;
		uniform float _cou_AU27_527;
		uniform float _cou_AU21_2021;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _metallicSmoothness;
		uniform float4 _metallicSmoothness_ST;
		uniform float _SmoothnessIntensity;
		uniform sampler2D _AO;
		uniform float4 _AO_ST;
		uniform float _AOIntensity;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Nomalmapbase = i.uv_texcoord * _Nomalmapbase_ST.xy + _Nomalmapbase_ST.zw;
			float2 uv_TextureArray0 = i.uv_texcoord * _TextureArray0_ST.xy + _TextureArray0_ST.zw;
			float3 texArray489 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)0)  ) );
			float4 appendResult27 = (float4(_cou_AU27_527 , _cou_AU27_527 , 1.0 , 0));
			float3 texArray491 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)1)  ) );
			float4 appendResult35 = (float4(_cou_AU21_2021 , _cou_AU21_2021 , 1.0 , 0));
			o.Normal = BlendNormals( UnpackNormal( tex2D( _Nomalmapbase, uv_Nomalmapbase ) ) , BlendNormals( ( float4( texArray489 , 0.0 ) * appendResult27 ).xyz , ( float4( texArray491 , 0.0 ) * appendResult35 ).xyz ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			o.Albedo = tex2D( _Albedo, uv_Albedo ).rgb;
			float2 uv_metallicSmoothness = i.uv_texcoord * _metallicSmoothness_ST.xy + _metallicSmoothness_ST.zw;
			float4 tex2DNode155 = tex2D( _metallicSmoothness, uv_metallicSmoothness );
			o.Metallic = tex2DNode155.r;
			o.Smoothness = ( tex2DNode155.a * _SmoothnessIntensity );
			float2 uv_AO = i.uv_texcoord * _AO_ST.xy + _AO_ST.zw;
			float4 temp_cast_8 = (_AOIntensity).xxxx;
			float4 clampResult546 = clamp( ( tex2D( _AO, uv_AO ) - temp_cast_8 ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			o.Occlusion = clampResult546.r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
1927;29;1906;1004;690.78;-243.206;1.069775;True;True
Node;AmplifyShaderEditor.CommentaryNode;31;-2221.766,168.931;Float;False;732.6843;607.697;Front Nez;10;26;36;27;35;521;522;38;28;29;37;;1,1,1,1;0;0
Node;AmplifyShaderEditor.IntNode;498;-2827.378,554.867;Float;False;Constant;_Int1;Int 1;33;0;Create;True;0;1;0;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;490;-2849.189,325.0919;Float;False;Constant;_Int0;Int 0;33;0;Create;True;0;0;0;0;1;INT;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-2205.633,534.0764;Float;False;Property;_cou_AU21_2021;cou_AU21_20-21;1;0;Create;True;0;1;0;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-2203.788,306.9317;Float;False;Property;_cou_AU27_527;cou_AU27_5-27;0;0;Create;True;0;1;0;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;489;-2648.195,257.2232;Float;True;Property;_TextureArray0;Texture Array 0;8;0;Create;True;0;None;0;Object;-1;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;29;-2143.487,386.931;Float;False;Constant;_Zvalue;Z value;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-2080.331,613.076;Float;False;Constant;_Float0;Float 0;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;491;-2644.438,473.9165;Float;True;Property;_TextureArray1;Texture Array 1;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;89;-757.224,204.6403;Float;False;418.3325;567.7946;blend base map;4;132;530;529;152;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;521;-2181.322,265.4918;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;152;-713.3385,279.1802;Float;True;Property;_Nomalmapbase;Nomalmap base;3;0;Create;True;0;None;073f50a0158ffcf4eaac4886ca0181b6;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;27;-1895.067,320.571;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;522;-2171.322,505.4916;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;35;-1888.911,539.7166;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;47;-1316.329,302.4086;Float;False;382.7109;340.7483;Combine  normalmap;1;45;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;156;-239.3705,446.8552;Float;True;Property;_AO;AO;6;0;Create;True;0;None;0835a7ad6ec37d7409ef8678f5ec60ed;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;542;-216.8942,645.2178;Float;False;Property;_AOIntensity;AO Intensity;7;0;Create;True;0;0;0;-1;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1696.489,223.9308;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-1698.333,451.0768;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;530;-434.7745,450.5114;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;529;-743.996,480.8556;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;543;108.128,387.5734;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;155;-201.31,793.6373;Float;True;Property;_metallicSmoothness;metallicSmoothness;4;0;Create;True;0;None;136e5e421e571a04d8d81d87e775e53b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendNormalsNode;45;-1252.199,349.1436;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;153;-332.0218,-181.3691;Float;False;378;280;Comment;1;1;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;545;-84.84171,1062.534;Float;False;Property;_SmoothnessIntensity;Smoothness Intensity;5;0;Create;True;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;546;282.9219,391.5309;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;544;262.5901,960.9384;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;132;-701.8198,503.8428;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;1;-282.0218,-131.3691;Float;True;Property;_Albedo;Albedo;2;0;Create;True;0;None;a3e03f0fcad61b144842fb86bdbc0d15;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;535.6907,18.9625;Float;False;True;3;Float;ASEMaterialInspector;0;0;Standard;Custom/WrinklesMapsNeck;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;489;1;490;0
WireConnection;491;1;498;0
WireConnection;521;0;489;0
WireConnection;27;0;28;0
WireConnection;27;1;28;0
WireConnection;27;2;29;0
WireConnection;522;0;491;0
WireConnection;35;0;38;0
WireConnection;35;1;38;0
WireConnection;35;2;37;0
WireConnection;26;0;521;0
WireConnection;26;1;27;0
WireConnection;36;0;522;0
WireConnection;36;1;35;0
WireConnection;530;0;152;0
WireConnection;529;0;530;0
WireConnection;543;0;156;0
WireConnection;543;1;542;0
WireConnection;45;0;26;0
WireConnection;45;1;36;0
WireConnection;546;0;543;0
WireConnection;544;0;155;4
WireConnection;544;1;545;0
WireConnection;132;0;529;0
WireConnection;132;1;45;0
WireConnection;0;0;1;0
WireConnection;0;1;132;0
WireConnection;0;3;155;0
WireConnection;0;4;544;0
WireConnection;0;5;546;0
ASEEND*/
//CHKSM=FEEF217DE5B8CFC19D724631DCA13BFBBA56C72C