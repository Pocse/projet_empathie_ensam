// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/WrinklesMapsHead"
{
	Properties
	{
		_front_AU4_415("front_AU4_4-15", Range( 0 , 4)) = 1
		_front_527("front_5-27", Range( 0 , 4)) = 1
		_front_612("front_6-12", Range( 0 , 4)) = 1
		_front_AU9_7910("front_AU9_7-9-10", Range( 0 , 4)) = 1
		_front_AU2_172("front_AU2_17-2", Range( 0 , 4)) = 1
		_front_2021("front_20-21", Range( 0 , 4)) = 1
		_front_23("front_23", Range( 0 , 4)) = 1
		_front_2526("front_25-26", Range( 0 , 4)) = 1
		_yeux_415("yeux_4-15", Range( 0 , 4)) = 1
		_yeux_AU5_527("yeux_AU5_5-27", Range( 0 , 4)) = 1
		_yeux_AU6_612("yeux_AU6_6-12", Range( 0 , 4)) = 1
		_yeux_AU7_7910("yeux_AU7_7-9-10", Range( 0 , 4)) = 1
		_yeux_172("yeux_17-2", Range( 0 , 4)) = 1
		_yeux_2021("yeux_20-21", Range( 0 , 4)) = 1
		_yeux_23("yeux_23", Range( 0 , 4)) = 1
		_yeux_2526("yeux_25-26", Range( 0 , 4)) = 1
		_bouche_AU15_415("bouche_AU15_4-15", Range( 0 , 4)) = 1
		_bouche_AU27_527("bouche_AU27_5-27", Range( 0 , 4)) = 1
		_bouche_AU12_612("bouche_AU12_6-12", Range( 0 , 4)) = 1
		_bouche_AU10_7910("bouche_AU10_7-9-10", Range( 0 , 4)) = 1
		_bouche_AU17_172("bouche_AU17_17-2", Range( 0 , 4)) = 1
		_bouche_AU20_2021("bouche_AU20_20-21", Range( 0 , 4)) = 1
		_bouche_AU23_23("bouche_AU23_23", Range( 0 , 4)) = 1
		_bouche_AU25_2526("bouche_AU25_25-26", Range( 0 , 4)) = 1
		_Albedo("Albedo", 2D) = "white" {}
		_Nomalmapbase("Nomalmap base", 2D) = "bump" {}
		_metallicSmoothness("metallicSmoothness", 2D) = "white" {}
		_SmoothnessIntensity("Smoothness Intensity", Range( 0 , 1)) = 1
		_AO("AO", 2D) = "white" {}
		_AOIntensity("AO Intensity", Range( -1 , 0)) = 0
		_TextureArray0("Texture Array 0", 2DArray ) = "" {}
		_RGB_mask("RGB_mask", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.5
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Nomalmapbase;
		uniform float4 _Nomalmapbase_ST;
		uniform UNITY_DECLARE_TEX2DARRAY( _TextureArray0 );
		uniform float4 _TextureArray0_ST;
		uniform float _front_AU4_415;
		uniform float _front_527;
		uniform float _front_612;
		uniform float _front_AU9_7910;
		uniform float _front_AU2_172;
		uniform float _front_2021;
		uniform float _front_23;
		uniform float _front_2526;
		uniform sampler2D _RGB_mask;
		uniform float4 _RGB_mask_ST;
		uniform float _yeux_415;
		uniform float _yeux_AU5_527;
		uniform float _yeux_AU6_612;
		uniform float _yeux_AU7_7910;
		uniform float _yeux_172;
		uniform float _yeux_2021;
		uniform float _yeux_23;
		uniform float _yeux_2526;
		uniform float _bouche_AU15_415;
		uniform float _bouche_AU27_527;
		uniform float _bouche_AU12_612;
		uniform float _bouche_AU10_7910;
		uniform float _bouche_AU17_172;
		uniform float _bouche_AU20_2021;
		uniform float _bouche_AU23_23;
		uniform float _bouche_AU25_2526;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _metallicSmoothness;
		uniform float4 _metallicSmoothness_ST;
		uniform float _SmoothnessIntensity;
		uniform sampler2D _AO;
		uniform float4 _AO_ST;
		uniform float _AOIntensity;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Nomalmapbase = i.uv_texcoord * _Nomalmapbase_ST.xy + _Nomalmapbase_ST.zw;
			float4 appendResult95 = (float4(0.0 , 0.0 , 1.0 , 0));
			float2 uv_TextureArray0 = i.uv_texcoord * _TextureArray0_ST.xy + _TextureArray0_ST.zw;
			float3 texArray489 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)0)  ) );
			float4 appendResult27 = (float4(_front_AU4_415 , _front_AU4_415 , 1.0 , 0));
			float3 texArray491 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)1)  ) );
			float4 appendResult35 = (float4(_front_527 , _front_527 , 1.0 , 0));
			float3 texArray492 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)2)  ) );
			float4 appendResult40 = (float4(_front_612 , _front_612 , 1.0 , 0));
			float3 texArray493 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)3)  ) );
			float4 appendResult265 = (float4(_front_AU9_7910 , _front_AU9_7910 , 1.0 , 0));
			float3 texArray494 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)4)  ) );
			float4 appendResult267 = (float4(_front_AU2_172 , _front_AU2_172 , 1.0 , 0));
			float3 texArray495 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)5)  ) );
			float4 appendResult256 = (float4(_front_2021 , _front_2021 , 1.0 , 0));
			float3 texArray496 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)6)  ) );
			float4 appendResult350 = (float4(_front_23 , _front_23 , 1.0 , 0));
			float3 texArray497 = UnpackNormal( UNITY_SAMPLE_TEX2DARRAY(_TextureArray0, float3(uv_TextureArray0, (float)7)  ) );
			float4 appendResult354 = (float4(_front_2526 , _front_2526 , 1.0 , 0));
			float2 uv_RGB_mask = i.uv_texcoord * _RGB_mask_ST.xy + _RGB_mask_ST.zw;
			float4 tex2DNode33 = tex2D( _RGB_mask, uv_RGB_mask );
			float4 lerpResult96 = lerp( appendResult95 , float4( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( ( float4( texArray489 , 0.0 ) * appendResult27 ).xyz , ( float4( texArray491 , 0.0 ) * appendResult35 ).xyz ) , ( float4( texArray492 , 0.0 ) * appendResult40 ).xyz ) , ( float4( texArray493 , 0.0 ) * appendResult265 ).xyz ) , ( float4( texArray494 , 0.0 ) * appendResult267 ).xyz ) , ( float4( texArray495 , 0.0 ) * appendResult256 ).xyz ) , ( float4( texArray496 , 0.0 ) * appendResult350 ).xyz ) , ( float4( texArray497 , 0.0 ) * appendResult354 ).xyz ) , 0.0 ) , tex2DNode33.g);
			float4 appendResult469 = (float4(0.0 , 0.0 , 1.0 , 0));
			float4 appendResult392 = (float4(_yeux_415 , _yeux_415 , 1.0 , 0));
			float4 appendResult387 = (float4(_yeux_AU5_527 , _yeux_AU5_527 , 1.0 , 0));
			float4 appendResult383 = (float4(_yeux_AU6_612 , _yeux_AU6_612 , 1.0 , 0));
			float4 appendResult390 = (float4(_yeux_AU7_7910 , _yeux_AU7_7910 , 1.0 , 0));
			float4 appendResult373 = (float4(_yeux_172 , _yeux_172 , 1.0 , 0));
			float4 appendResult399 = (float4(_yeux_2021 , _yeux_2021 , 1.0 , 0));
			float4 appendResult403 = (float4(_yeux_23 , _yeux_23 , 1.0 , 0));
			float4 appendResult407 = (float4(_yeux_2526 , _yeux_2526 , 1.0 , 0));
			float4 lerpResult467 = lerp( appendResult469 , float4( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( ( float4( texArray489 , 0.0 ) * appendResult392 ).xyz , ( float4( texArray491 , 0.0 ) * appendResult387 ).xyz ) , ( float4( texArray492 , 0.0 ) * appendResult383 ).xyz ) , ( float4( texArray493 , 0.0 ) * appendResult390 ).xyz ) , ( float4( texArray494 , 0.0 ) * appendResult373 ).xyz ) , ( float4( texArray495 , 0.0 ) * appendResult399 ).xyz ) , ( float4( texArray496 , 0.0 ) * appendResult403 ).xyz ) , ( float4( texArray497 , 0.0 ) * appendResult407 ).xyz ) , 0.0 ) , tex2DNode33.r);
			float4 appendResult476 = (float4(0.0 , 0.0 , 1.0 , 0));
			float4 appendResult446 = (float4(_bouche_AU15_415 , _bouche_AU15_415 , 1.0 , 0));
			float4 appendResult441 = (float4(_bouche_AU27_527 , _bouche_AU27_527 , 1.0 , 0));
			float4 appendResult437 = (float4(_bouche_AU12_612 , _bouche_AU12_612 , 1.0 , 0));
			float4 appendResult444 = (float4(_bouche_AU10_7910 , _bouche_AU10_7910 , 1.0 , 0));
			float4 appendResult427 = (float4(_bouche_AU17_172 , _bouche_AU17_172 , 1.0 , 0));
			float4 appendResult453 = (float4(_bouche_AU20_2021 , _bouche_AU20_2021 , 1.0 , 0));
			float4 appendResult457 = (float4(_bouche_AU23_23 , _bouche_AU23_23 , 1.0 , 0));
			float4 appendResult461 = (float4(_bouche_AU25_2526 , _bouche_AU25_2526 , 1.0 , 0));
			float4 lerpResult474 = lerp( appendResult476 , float4( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( BlendNormals( ( float4( texArray489 , 0.0 ) * appendResult446 ).xyz , ( float4( texArray491 , 0.0 ) * appendResult441 ).xyz ) , ( float4( texArray492 , 0.0 ) * appendResult437 ).xyz ) , ( float4( texArray493 , 0.0 ) * appendResult444 ).xyz ) , ( float4( texArray494 , 0.0 ) * appendResult427 ).xyz ) , ( float4( texArray495 , 0.0 ) * appendResult453 ).xyz ) , ( float4( texArray496 , 0.0 ) * appendResult457 ).xyz ) , ( float4( texArray497 , 0.0 ) * appendResult461 ).xyz ) , 0.0 ) , tex2DNode33.b);
			o.Normal = BlendNormals( UnpackNormal( tex2D( _Nomalmapbase, uv_Nomalmapbase ) ) , BlendNormals( BlendNormals( lerpResult96.xyz , lerpResult467.xyz ) , lerpResult474.xyz ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			o.Albedo = tex2D( _Albedo, uv_Albedo ).rgb;
			float2 uv_metallicSmoothness = i.uv_texcoord * _metallicSmoothness_ST.xy + _metallicSmoothness_ST.zw;
			float4 tex2DNode155 = tex2D( _metallicSmoothness, uv_metallicSmoothness );
			o.Metallic = tex2DNode155.r;
			o.Smoothness = ( tex2DNode155.a * _SmoothnessIntensity );
			float2 uv_AO = i.uv_texcoord * _AO_ST.xy + _AO_ST.zw;
			float4 temp_cast_64 = (_AOIntensity).xxxx;
			float4 clampResult546 = clamp( ( tex2D( _AO, uv_AO ) - temp_cast_64 ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			o.Occlusion = clampResult546.r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14501
1927;29;1906;1004;5061.296;69.21777;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;31;-4368.73,-2527.301;Float;False;728.967;1864.151;Front Nez;40;267;263;261;259;258;257;256;266;265;262;260;35;28;27;42;38;29;37;26;41;43;36;40;349;350;351;352;353;354;355;356;357;521;522;523;524;525;526;527;528;;1,1,1,1;0;0
Node;AmplifyShaderEditor.IntNode;498;-5435.285,-193.4898;Float;False;Constant;_Int1;Int 1;33;0;Create;True;0;1;0;0;1;INT;0
Node;AmplifyShaderEditor.IntNode;490;-5457.096,-423.2647;Float;False;Constant;_Int0;Int 0;33;0;Create;True;0;0;0;0;1;INT;0
Node;AmplifyShaderEditor.TextureArrayNode;489;-5256.102,-491.1334;Float;True;Property;_TextureArray0;Texture Array 0;30;0;Create;True;0;None;0;Object;-1;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;38;-4352.596,-2162.155;Float;False;Property;_front_527;front_5-27;1;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;491;-5252.345,-274.4401;Float;True;Property;_TextureArray1;Texture Array 1;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;37;-4227.295,-2083.155;Float;False;Constant;_Float0;Float 0;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-4290.45,-2309.301;Float;False;Constant;_Zvalue;Z value;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-4350.752,-2389.3;Float;False;Property;_front_AU4_415;front_AU4_4-15;0;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;35;-4035.875,-2156.515;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;27;-4042.031,-2375.661;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;522;-4382.285,-2254.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;521;-4392.285,-2494.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;359;-4359.4,-632.1289;Float;False;728.967;1864.151;YEUX;40;410;409;408;407;406;405;404;403;402;401;400;399;398;397;396;395;394;393;392;391;390;389;388;387;386;385;384;383;382;375;374;373;513;514;515;516;517;518;519;520;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-3845.296,-2245.155;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-3843.452,-2472.301;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.IntNode;499;-5441.285,15.51049;Float;False;Constant;_Int2;Int 2;33;0;Create;True;0;2;0;0;1;INT;0
Node;AmplifyShaderEditor.CommentaryNode;47;-3615.953,-2522.823;Float;False;390.8237;1841.614;Combine  normalmap;20;293;45;278;279;276;46;277;292;290;291;289;288;282;285;281;280;287;286;284;283;;1,1,1,1;0;0
Node;AmplifyShaderEditor.BlendNormalsNode;45;-3551.823,-2476.088;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;397;-4343.266,-266.983;Float;False;Property;_yeux_AU5_527;yeux_AU5_5-27;9;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-4352.598,-1939.149;Float;False;Property;_front_612;front_6-12;2;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;393;-4217.964,-187.9842;Float;False;Constant;_Float19;Float 19;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;492;-5246.345,-75.44019;Float;True;Property;_TextureArray2;Texture Array 2;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;395;-4216.12,-415.129;Float;False;Constant;_Float20;Float 20;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;42;-4227.297,-1860.149;Float;False;Constant;_Float2;Float 2;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;388;-4341.422,-494.129;Float;False;Property;_yeux_415;yeux_4-15;8;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;519;-4383.124,-375.5397;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;520;-4405.124,-611.5396;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;523;-4379.285,-2030.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;40;-4035.878,-1933.509;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;277;-3420.718,-2356.05;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;392;-4024.701,-488.4891;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;387;-4026.545,-261.3441;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;384;-3835.965,-349.9843;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;-3845.299,-2022.149;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;358;-3606.623,-627.6521;Float;False;390.8237;1841.614;Combine  normalmap;19;381;380;379;378;377;376;372;371;370;369;368;367;366;365;364;363;362;361;360;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;276;-3685.576,-2354.803;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;413;-4360.92,1257.214;Float;False;728.967;1864.151;Bouche;40;464;463;462;461;460;459;458;457;456;455;454;453;452;451;450;449;448;447;446;445;444;443;442;441;440;439;438;437;436;429;428;427;511;512;510;509;508;506;505;507;;1,1,1,1;0;0
Node;AmplifyShaderEditor.IntNode;500;-5426.285,173.5105;Float;False;Constant;_Int3;Int 3;33;0;Create;True;0;3;0;0;1;INT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;394;-3834.122,-577.129;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;451;-4344.786,1622.36;Float;False;Property;_bouche_AU27_527;bouche_AU27_5-27;17;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;266;-4351.335,-1717.664;Float;False;Property;_front_AU9_7910;front_AU9_7-9-10;3;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;386;-4343.268,-43.9773;Float;False;Property;_yeux_AU6_612;yeux_AU6_6-12;10;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;262;-4226.032,-1638.664;Float;False;Constant;_Float4;Float 4;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;447;-4219.484,1701.359;Float;False;Constant;_Float53;Float 53;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;449;-4217.64,1474.214;Float;False;Constant;_Float54;Float 54;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;381;-3542.493,-580.9161;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureArrayNode;493;-5239.345,114.5598;Float;True;Property;_TextureArray3;Texture Array 3;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;442;-4342.942,1395.214;Float;False;Property;_bouche_AU15_415;bouche_AU15_4-15;16;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;46;-3556.063,-2230.58;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;396;-4217.966,35.02172;Float;False;Constant;_Float21;Float 21;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;446;-4026.221,1400.854;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;376;-3411.386,-460.8781;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;289;-3407.924,-2131.421;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;518;-4401.124,-157.5398;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;512;-4401.417,1258.178;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;524;-4388.285,-1816.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;441;-4028.065,1627.999;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;511;-4412.336,1446.672;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;265;-4034.608,-1712.024;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;383;-4026.548,-38.33814;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;385;-3835.968,-126.9783;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.IntNode;501;-5409.285,378.5105;Float;False;Constant;_Int4;Int 4;33;0;Create;True;0;4;0;0;1;INT;0
Node;AmplifyShaderEditor.CommentaryNode;412;-3608.143,1261.691;Float;False;390.8237;1841.614;Combine normalmap;19;435;434;433;432;431;430;426;425;424;423;422;421;420;419;418;417;416;415;414;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;378;-3676.246,-459.6322;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;288;-3686.226,-2093.821;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;260;-3844.028,-1800.664;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;448;-3835.642,1312.214;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;438;-3837.485,1539.359;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;440;-4344.788,1845.367;Float;False;Property;_bouche_AU12_612;bouche_AU12_6-12;18;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;278;-3558.006,-1972.12;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;391;-4216.702,256.5081;Float;False;Constant;_Float18;Float 18;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;450;-4219.486,1924.365;Float;False;Constant;_Float55;Float 55;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;435;-3544.013,1308.427;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;261;-4227.878,-1411.519;Float;False;Constant;_Float3;Float 3;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;382;-4342.005,177.5081;Float;False;Property;_yeux_AU7_7910;yeux_AU7_7-9-10;11;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;263;-4353.177,-1488.518;Float;False;Property;_front_AU2_172;front_AU2_17-2;4;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;377;-3546.733,-335.4081;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureArrayNode;494;-5242.345,309.5594;Float;True;Property;_TextureArray4;Texture Array 4;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;517;-4414.124,79.46021;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;437;-4028.068,1851.005;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;390;-4025.278,183.1467;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;430;-3412.906,1428.465;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;525;-4395.285,-1589.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;280;-3399.047,-1870.164;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;369;-3398.593,-236.25;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;267;-4036.451,-1484.879;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;510;-4404.096,1694.974;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;389;-3834.698,94.50805;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;432;-3677.766,1429.711;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;368;-3676.896,-198.6502;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.IntNode;502;-5401.285,587.5105;Float;False;Constant;_Int5;Int 5;33;0;Create;True;0;5;0;0;1;INT;0
Node;AmplifyShaderEditor.WireNode;281;-3672.779,-1825.49;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;257;-3845.872,-1573.518;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;439;-3837.488,1762.365;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;258;-4353.179,-1267.512;Float;False;Property;_front_2021;front_20-21;5;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;279;-3562.247,-1726.614;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;349;-4223.074,-1179.029;Float;False;Constant;_Float6;Float 6;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;495;-5252.258,509.6311;Float;True;Property;_TextureArray5;Texture Array 5;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;445;-4218.222,2145.851;Float;False;Constant;_Float52;Float 52;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;374;-4343.847,404.6531;Float;False;Property;_yeux_172;yeux_17-2;12;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;398;-4218.547,483.6516;Float;False;Constant;_Float23;Float 23;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;380;-3548.675,-76.94898;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;436;-4343.525,2066.851;Float;False;Property;_bouche_AU10_7910;bouche_AU10_7-9-10;19;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;431;-3548.253,1553.935;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;256;-4036.454,-1261.873;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;444;-4026.798,2072.49;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;516;-4395.124,304.4602;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;509;-4398.676,1918.87;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;526;-4383.285,-1362.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;373;-4027.121,410.2917;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;423;-3400.112,1653.093;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;287;-3426.164,-1623.837;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;364;-3423.716,0.008049011;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;443;-3836.217,1983.851;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;422;-3678.416,1690.693;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;375;-3836.542,321.6531;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;365;-3663.448,69.68089;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.IntNode;503;-5416.285,782.5099;Float;False;Constant;_Int6;Int 6;33;0;Create;True;0;6;0;0;1;INT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;259;-3852.779,-1333.942;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;286;-3679.958,-1579.972;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;351;-4353.951,-1050.837;Float;False;Property;_front_23;front_23;6;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;353;-4223.846,-962.353;Float;False;Constant;_Float11;Float 11;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;402;-4213.744,716.1417;Float;False;Constant;_Float25;Float 25;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;282;-3560.287,-1459.062;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;452;-4220.067,2372.995;Float;False;Constant;_Float57;Float 57;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;400;-4343.849,627.6589;Float;False;Property;_yeux_2021;yeux_20-21;13;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;496;-5256.179,712.379;Float;True;Property;_TextureArray6;Texture Array 6;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendNormalsNode;379;-3552.917,168.5569;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BlendNormalsNode;434;-3550.195,1812.394;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;428;-4345.367,2293.996;Float;False;Property;_bouche_AU17_172;bouche_AU17_17-2;20;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;515;-4407.124,524.4602;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;363;-3416.833,271.3338;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;527;-4380.285,-1154.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;418;-3425.236,1889.351;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;350;-4037.225,-1045.198;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;508;-4402.279,2136.787;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;399;-4027.124,633.2981;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;284;-3423.021,-1366.103;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;427;-4028.641,2299.635;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;401;-3843.448,561.2288;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;285;-3659.06,-1296.431;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;419;-3664.968,1959.024;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;352;-3853.554,-1117.267;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.IntNode;504;-5431.285,971.5084;Float;False;Constant;_Int7;Int 7;33;0;Create;True;0;7;0;0;1;INT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;429;-3838.062,2210.996;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;362;-3670.629,315.199;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;454;-4345.369,2517.002;Float;False;Property;_bouche_AU20_2021;bouche_AU20_20-21;21;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;355;-4357.193,-837.8979;Float;False;Property;_front_2526;front_25-26;7;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;406;-4214.516,932.8165;Float;False;Constant;_Float27;Float 27;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;283;-3564.528,-1213.553;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;357;-4227.088,-749.415;Float;False;Constant;_Float13;Float 13;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;433;-3554.437,2057.9;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;404;-4344.621,844.3336;Float;False;Property;_yeux_23;yeux_23;14;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;456;-4215.264,2605.485;Float;False;Constant;_Float59;Float 59;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureArrayNode;497;-5251.379,925.1774;Float;True;Property;_TextureArray7;Texture Array 7;32;0;Create;True;0;None;0;Instance;489;Auto;True;7;6;SAMPLER2D;;False;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendNormalsNode;367;-3550.957,436.1086;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;361;-3413.69,529.0676;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;417;-3418.353,2160.677;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;292;-3425.323,-1108.107;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;403;-4027.895,849.9728;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;514;-4389.124,749.4601;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;453;-4028.644,2522.641;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;507;-4392.139,2340.653;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;354;-4040.468,-832.259;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;528;-4376.285,-930.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;405;-3844.224,777.9039;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;366;-3649.729,598.74;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;416;-3672.149,2204.542;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;455;-3844.968,2450.572;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;356;-3856.794,-904.3281;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;291;-3667.482,-1035.56;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;460;-4216.036,2822.16;Float;False;Constant;_Float61;Float 61;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;458;-4346.141,2733.677;Float;False;Property;_bouche_AU23_23;bouche_AU23_23;22;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;408;-4347.863,1057.273;Float;False;Property;_yeux_2526;yeux_25-26;15;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;290;-3570.275,-948.3882;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BlendNormalsNode;360;-3555.198,681.6178;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;410;-4217.757,1145.756;Float;False;Constant;_Float29;Float 29;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;421;-3552.477,2325.452;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;415;-3415.209,2418.411;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;293;-3350.588,-1112.333;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;372;-3415.992,787.0636;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;465;-3000.678,-548.7291;Float;False;600.8996;290.663;;4;467;469;471;470;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;407;-4031.138,1062.912;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;506;-4414.022,2579.779;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;513;-4387.124,956.46;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;98;-3024.465,-1871.549;Float;False;589.7294;315.3828;Mask;5;96;295;95;93;94;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;457;-4029.415,2739.316;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;370;-3658.152,859.6095;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;94;-3004.544,-1751.819;Float;False;Constant;_Float36;Float 36;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;294;-3085.451,-1650.462;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;420;-3651.25,2488.083;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;459;-3845.744,2667.247;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;471;-2980.757,-428.9981;Float;False;Constant;_Float65;Float 65;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;93;-3005.845,-1824.819;Float;False;Constant;_Float35;Float 35;5;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;409;-3847.464,990.8429;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;470;-2982.058,-501.9981;Float;False;Constant;_Float64;Float 64;5;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;295;-2871.157,-1703.55;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;462;-4349.383,2946.616;Float;False;Property;_bouche_AU25_2526;bouche_AU25_25-26;23;0;Create;True;0;1;0;0;4;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;371;-3560.945,946.7828;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;464;-4219.277,3035.099;Float;False;Constant;_Float63;Float 63;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;95;-2855.124,-1820.18;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;469;-2831.337,-497.3602;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BlendNormalsNode;414;-3556.717,2570.961;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;33;-3115.791,-1041.509;Float;True;Property;_RGB_mask;RGB_mask;31;0;Create;True;0;2d64373fe65c47543b0afc7789012b57;2d64373fe65c47543b0afc7789012b57;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;505;-4389.681,2782.173;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;472;-2993.098,1505.189;Float;False;607.2661;288.0314;Mask ;4;474;476;478;477;;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;461;-4032.658,2952.255;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;48;-1172.328,209.8072;Float;False;387.1638;568.0553;blend all wrinkles;4;145;5;125;146;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;426;-3417.511,2676.406;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;96;-2673.769,-1824.379;Float;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;467;-2649.982,-501.5582;Float;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;424;-3659.672,2748.953;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;463;-3848.984,2880.186;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BlendNormalsNode;5;-1112.657,276.8141;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;478;-2973.177,1624.92;Float;False;Constant;_Float67;Float 67;7;0;Create;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;89;-757.224,204.6403;Float;False;418.3325;567.7946;blend base map;4;132;530;529;152;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;477;-2974.478,1551.92;Float;False;Constant;_Float66;Float 66;5;0;Create;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;145;-913.5703,393.4322;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;152;-713.3385,279.1802;Float;True;Property;_Nomalmapbase;Nomalmap base;25;0;Create;True;0;None;50201a6040e547b49ad852f011b1309f;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;476;-2823.757,1556.559;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BlendNormalsNode;425;-3562.465,2836.126;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;530;-498.7745,386.5114;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;156;-252.877,443.1494;Float;True;Property;_AO;AO;28;0;Create;True;0;None;e0f744fa1a1dd6240a1711206d920f17;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;542;-224.447,646.538;Float;False;Property;_AOIntensity;AO Intensity;29;0;Create;True;0;0;-0.4437248;-1;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;146;-1223.131,416.9832;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;474;-2642.402,1552.36;Float;True;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;545;-84.84171,1062.534;Float;False;Property;_SmoothnessIntensity;Smoothness Intensity;27;0;Create;True;0;1;0.569;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;125;-1097.903,511.2328;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;155;-201.31,793.6373;Float;True;Property;_metallicSmoothness;metallicSmoothness;26;0;Create;True;0;None;551c474511bc5e34db02c22317993f89;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;529;-807.996,416.8556;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;153;-332.0218,-181.3691;Float;False;378;280;Comment;1;1;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;543;64.4231,456.4804;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;546;221.9349,449.9846;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;544;262.5901,960.9384;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;132;-701.8198,503.8428;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;1;-282.0218,-131.3691;Float;True;Property;_Albedo;Albedo;24;0;Create;True;0;None;d6f7615b1075f7645993129536ec8195;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;535.6907,18.9625;Float;False;True;3;Float;ASEMaterialInspector;0;0;Standard;Custom/WrinklesMapsHead;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;489;1;490;0
WireConnection;491;1;498;0
WireConnection;35;0;38;0
WireConnection;35;1;38;0
WireConnection;35;2;37;0
WireConnection;27;0;28;0
WireConnection;27;1;28;0
WireConnection;27;2;29;0
WireConnection;522;0;491;0
WireConnection;521;0;489;0
WireConnection;36;0;522;0
WireConnection;36;1;35;0
WireConnection;26;0;521;0
WireConnection;26;1;27;0
WireConnection;45;0;26;0
WireConnection;45;1;36;0
WireConnection;492;1;499;0
WireConnection;519;0;491;0
WireConnection;520;0;489;0
WireConnection;523;0;492;0
WireConnection;40;0;43;0
WireConnection;40;1;43;0
WireConnection;40;2;42;0
WireConnection;277;0;45;0
WireConnection;392;0;388;0
WireConnection;392;1;388;0
WireConnection;392;2;395;0
WireConnection;387;0;397;0
WireConnection;387;1;397;0
WireConnection;387;2;393;0
WireConnection;384;0;519;0
WireConnection;384;1;387;0
WireConnection;41;0;523;0
WireConnection;41;1;40;0
WireConnection;276;0;277;0
WireConnection;394;0;520;0
WireConnection;394;1;392;0
WireConnection;381;0;394;0
WireConnection;381;1;384;0
WireConnection;493;1;500;0
WireConnection;46;0;276;0
WireConnection;46;1;41;0
WireConnection;446;0;442;0
WireConnection;446;1;442;0
WireConnection;446;2;449;0
WireConnection;376;0;381;0
WireConnection;289;0;46;0
WireConnection;518;0;492;0
WireConnection;512;0;489;0
WireConnection;524;0;493;0
WireConnection;441;0;451;0
WireConnection;441;1;451;0
WireConnection;441;2;447;0
WireConnection;511;0;491;0
WireConnection;265;0;266;0
WireConnection;265;1;266;0
WireConnection;265;2;262;0
WireConnection;383;0;386;0
WireConnection;383;1;386;0
WireConnection;383;2;396;0
WireConnection;385;0;518;0
WireConnection;385;1;383;0
WireConnection;378;0;376;0
WireConnection;288;0;289;0
WireConnection;260;0;524;0
WireConnection;260;1;265;0
WireConnection;448;0;512;0
WireConnection;448;1;446;0
WireConnection;438;0;511;0
WireConnection;438;1;441;0
WireConnection;278;0;288;0
WireConnection;278;1;260;0
WireConnection;435;0;448;0
WireConnection;435;1;438;0
WireConnection;377;0;378;0
WireConnection;377;1;385;0
WireConnection;494;1;501;0
WireConnection;517;0;493;0
WireConnection;437;0;440;0
WireConnection;437;1;440;0
WireConnection;437;2;450;0
WireConnection;390;0;382;0
WireConnection;390;1;382;0
WireConnection;390;2;391;0
WireConnection;430;0;435;0
WireConnection;525;0;494;0
WireConnection;280;0;278;0
WireConnection;369;0;377;0
WireConnection;267;0;263;0
WireConnection;267;1;263;0
WireConnection;267;2;261;0
WireConnection;510;0;492;0
WireConnection;389;0;517;0
WireConnection;389;1;390;0
WireConnection;432;0;430;0
WireConnection;368;0;369;0
WireConnection;281;0;280;0
WireConnection;257;0;525;0
WireConnection;257;1;267;0
WireConnection;439;0;510;0
WireConnection;439;1;437;0
WireConnection;279;0;281;0
WireConnection;279;1;257;0
WireConnection;495;1;502;0
WireConnection;380;0;368;0
WireConnection;380;1;389;0
WireConnection;431;0;432;0
WireConnection;431;1;439;0
WireConnection;256;0;258;0
WireConnection;256;1;258;0
WireConnection;256;2;349;0
WireConnection;444;0;436;0
WireConnection;444;1;436;0
WireConnection;444;2;445;0
WireConnection;516;0;494;0
WireConnection;509;0;493;0
WireConnection;526;0;495;0
WireConnection;373;0;374;0
WireConnection;373;1;374;0
WireConnection;373;2;398;0
WireConnection;423;0;431;0
WireConnection;287;0;279;0
WireConnection;364;0;380;0
WireConnection;443;0;509;0
WireConnection;443;1;444;0
WireConnection;422;0;423;0
WireConnection;375;0;516;0
WireConnection;375;1;373;0
WireConnection;365;0;364;0
WireConnection;259;0;526;0
WireConnection;259;1;256;0
WireConnection;286;0;287;0
WireConnection;282;0;286;0
WireConnection;282;1;259;0
WireConnection;496;1;503;0
WireConnection;379;0;365;0
WireConnection;379;1;375;0
WireConnection;434;0;422;0
WireConnection;434;1;443;0
WireConnection;515;0;495;0
WireConnection;363;0;379;0
WireConnection;527;0;496;0
WireConnection;418;0;434;0
WireConnection;350;0;351;0
WireConnection;350;1;351;0
WireConnection;350;2;353;0
WireConnection;508;0;494;0
WireConnection;399;0;400;0
WireConnection;399;1;400;0
WireConnection;399;2;402;0
WireConnection;284;0;282;0
WireConnection;427;0;428;0
WireConnection;427;1;428;0
WireConnection;427;2;452;0
WireConnection;401;0;515;0
WireConnection;401;1;399;0
WireConnection;285;0;284;0
WireConnection;419;0;418;0
WireConnection;352;0;527;0
WireConnection;352;1;350;0
WireConnection;429;0;508;0
WireConnection;429;1;427;0
WireConnection;362;0;363;0
WireConnection;283;0;285;0
WireConnection;283;1;352;0
WireConnection;433;0;419;0
WireConnection;433;1;429;0
WireConnection;497;1;504;0
WireConnection;367;0;362;0
WireConnection;367;1;401;0
WireConnection;361;0;367;0
WireConnection;417;0;433;0
WireConnection;292;0;283;0
WireConnection;403;0;404;0
WireConnection;403;1;404;0
WireConnection;403;2;406;0
WireConnection;514;0;496;0
WireConnection;453;0;454;0
WireConnection;453;1;454;0
WireConnection;453;2;456;0
WireConnection;507;0;495;0
WireConnection;354;0;355;0
WireConnection;354;1;355;0
WireConnection;354;2;357;0
WireConnection;528;0;497;0
WireConnection;405;0;514;0
WireConnection;405;1;403;0
WireConnection;366;0;361;0
WireConnection;416;0;417;0
WireConnection;455;0;507;0
WireConnection;455;1;453;0
WireConnection;356;0;528;0
WireConnection;356;1;354;0
WireConnection;291;0;292;0
WireConnection;290;0;291;0
WireConnection;290;1;356;0
WireConnection;360;0;366;0
WireConnection;360;1;405;0
WireConnection;421;0;416;0
WireConnection;421;1;455;0
WireConnection;415;0;421;0
WireConnection;293;0;290;0
WireConnection;372;0;360;0
WireConnection;407;0;408;0
WireConnection;407;1;408;0
WireConnection;407;2;410;0
WireConnection;506;0;496;0
WireConnection;513;0;497;0
WireConnection;457;0;458;0
WireConnection;457;1;458;0
WireConnection;457;2;460;0
WireConnection;370;0;372;0
WireConnection;294;0;293;0
WireConnection;420;0;415;0
WireConnection;459;0;506;0
WireConnection;459;1;457;0
WireConnection;409;0;513;0
WireConnection;409;1;407;0
WireConnection;295;0;294;0
WireConnection;371;0;370;0
WireConnection;371;1;409;0
WireConnection;95;0;93;0
WireConnection;95;1;93;0
WireConnection;95;2;94;0
WireConnection;469;0;470;0
WireConnection;469;1;470;0
WireConnection;469;2;471;0
WireConnection;414;0;420;0
WireConnection;414;1;459;0
WireConnection;505;0;497;0
WireConnection;461;0;462;0
WireConnection;461;1;462;0
WireConnection;461;2;464;0
WireConnection;426;0;414;0
WireConnection;96;0;95;0
WireConnection;96;1;295;0
WireConnection;96;2;33;2
WireConnection;467;0;469;0
WireConnection;467;1;371;0
WireConnection;467;2;33;1
WireConnection;424;0;426;0
WireConnection;463;0;505;0
WireConnection;463;1;461;0
WireConnection;5;0;96;0
WireConnection;5;1;467;0
WireConnection;145;0;5;0
WireConnection;476;0;477;0
WireConnection;476;1;477;0
WireConnection;476;2;478;0
WireConnection;425;0;424;0
WireConnection;425;1;463;0
WireConnection;530;0;152;0
WireConnection;146;0;145;0
WireConnection;474;0;476;0
WireConnection;474;1;425;0
WireConnection;474;2;33;3
WireConnection;125;0;146;0
WireConnection;125;1;474;0
WireConnection;529;0;530;0
WireConnection;543;0;156;0
WireConnection;543;1;542;0
WireConnection;546;0;543;0
WireConnection;544;0;155;4
WireConnection;544;1;545;0
WireConnection;132;0;529;0
WireConnection;132;1;125;0
WireConnection;0;0;1;0
WireConnection;0;1;132;0
WireConnection;0;3;155;0
WireConnection;0;4;544;0
WireConnection;0;5;546;0
ASEEND*/
//CHKSM=F239341C14A436E71F3F3230E24D8F51CE4C7927