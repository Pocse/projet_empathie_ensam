﻿using RogoDigital.Lipsync;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueTest : MonoBehaviour{
   // public Dictionary<string,string> questionVal=new Dictionary<string,string>();
   //获取所有的Text的字符串
    private string qa1 = "Bonjour Mme Durand. Comment allez vous aujourd’hui ?";
    private string qb1 = "Bonjour Mme Durand. Dites moi comment vous sentez vous aujourd'hui ?";
    private string qa2 = "Votre hanche va t-elle mieux ?";
    private string qb2 = "Arrivez-vous à marcher depuis la séance de kinésithérapeute?";
    private string qa3 = "C’est une bonne nouvelle ! Êtes vous venue seule ?";
    private string qb3 = "C’est une bonne nouvelle ! Un de vos enfants vous a accompagné à ce rendez-vous?";
    private string qa4 = "C’est dommage, je voulais vous voir car j’ai reçu les résultats de la prise de sang réalisée à la suite de votre chute la semaine dernière.";
    private string qb4 = "J'aurais préféré que vous ne soyez pas seule, car j'ai reçu les résultats de votre prise de sang";
    private string qa5 = "Je sais que c’est compliqué à entendre, mais vous allez devoir être hospitalisée";
    private string qb5 = "Vous êtes probablement atteinte d’un cancer de l'estomac.. Je suis désolée";
    private string qa6 = "Vous allez être hospitalisée pendant quelques semaines, afin de réaliser des examens complémentaires afin que nous déterminions la nature exacte de votre pathologie. Nous suspectons un cancer de l’estomac.";
    private string qb6 = "C’est un cancer très peu répandu en France, il y a donc peu de prévention.Mais dans votre cas, il s’agit probablement d’un diagnostic précoce, ce qui facilite la prise en charge rapide.";
    private string qa7 = "La médecine a beaucoup évolué aujourd’hui, vous n’êtes pas nécessairement condamnée. Nous allons faire tout notre possible pour vous accompagner au mieux. ";
    private string qb7 = "Environ 25%  des personnes diagnostiquées d’un cancer de l’estomac survivent au moins 5 ans, et comme pour l’instant, le cancer ne s’est pas énormément développé dans votre corps, cela vous donne une grande chance de survie";
    private string qa8 = "Vous devriez les contacter, il est important d'être soutenu dans ces moments difficiles.";
    private string qb8 = "C'est à vous de prendre cette décision Madame.";
    private string qa9 = "Oui, bien sûr, mais les temps de visites seront limités afin que vous puissiez vous reposer.";
    private string qb9 = "Votre famille pourra venir vous voir si elle le souhaite, pendant les heures de visite autorisées.";
    private string qa10 = "C’est possible oui, mais dans ce cas, vous et votre famille serez accompagné afin de rendre cette étape moins douloureuse.";
    private string qb10 = "Vous pourriez oui, si jamais vos résultats ne s’améliorent pas. Mais vous allez être encadrée à chaque étape du traitement, ne vous en faite pas.";
    private string qa11 = "Nous verrons ce qu’il est possible de faire, si vos résultats sont corrects, vous pourrez sûrement sortir.";
    private string qb11= "J’ai peur que ce soit compliqué pour vous, c’est un traitement qui est fatiguant";

    
    //存储所有的声音 这个声音是选择完texte之后的声音
    [Header("Fichiers Audios Docteur First choices")]
    public AudioClip ac1_1;
    public AudioClip ac1_2;
    public AudioClip ac1_3;
    public AudioClip ac1_4;
    public AudioClip ac1_5;
    public AudioClip ac1_6;
    public AudioClip ac1_7;
    public AudioClip ac1_8;
    public AudioClip ac1_9;
    public AudioClip ac1_10;
    public AudioClip ac1_11;
    [Header("Fichiers Audios Docteur Second choices")]
    public AudioClip ac2_1;
    public AudioClip ac2_2;
    public AudioClip ac2_3;
    public AudioClip ac2_4;
    public AudioClip ac2_5;
    public AudioClip ac2_6;
    public AudioClip ac2_7;
    public AudioClip ac2_8;
    public AudioClip ac2_9;
    public AudioClip ac2_10;
    public AudioClip ac2_11;
    //存取所有LipSyncData声音和表情
    [Header("Fichiers LipSync")]
    public LipSyncData ls1;
    public LipSyncData ls2;
    public LipSyncData ls3;
    public LipSyncData ls4;
    public LipSyncData ls5;
    public LipSyncData ls6;
    public LipSyncData ls7;
    public LipSyncData ls8;
    public LipSyncData ls9;
    public LipSyncData ls10;
   
    //创建两个键值对负责展开对话框
    public Dictionary<int, string> Q1 = new Dictionary<int, string>();
    public Dictionary<int, string> Q2 = new Dictionary<int, string>();
    //键值对负责存取所有的声音
    public Dictionary<int, AudioClip> A1 = new Dictionary<int, AudioClip>();
    public Dictionary<int, AudioClip> A2 = new Dictionary<int, AudioClip>();
    //这个键值对负责存取所有LipSyncData的声音和动画

    public Dictionary<int, LipSyncData> LipData = new Dictionary<int, LipSyncData>();
    public Dictionary<int, AnimationClip> Anim = new Dictionary<int, AnimationClip>();
    private void Awake()
    {
        //存储第1个text
        Q1.Add(0,qa1);
        Q1.Add(1,qa2);
        Q1.Add(2,qa3);
        Q1.Add(3,qa4);
        Q1.Add(4,qa5);
        Q1.Add(5,qa6);
        Q1.Add(6,qa7);
        Q1.Add(7,qa8);
        Q1.Add(8,qa9);
        Q1.Add(9,qa10);
        Q1.Add(10,qa11);

        //存储第2个text
        Q2.Add(0,qb1);
        Q2.Add(1,qb2);
        Q2.Add(2,qb3);
        Q2.Add(3,qb4);
        Q2.Add(4,qb5);
        Q2.Add(5,qb6);
        Q2.Add(6,qb7);
        Q2.Add(7,qb8);
        Q2.Add(8,qb9);
        Q2.Add(9,qb10);
        Q2.Add(10,qb11);

        //存储audio第一个的选项的录音
        A1.Add(0, ac1_1);
        A1.Add(1, ac1_2);
        A1.Add(2, ac1_3);
        A1.Add(3, ac1_4);
        A1.Add(4, ac1_5);
        A1.Add(5, ac1_6);
        A1.Add(6, ac1_7);
        A1.Add(7, ac1_8);
        A1.Add(8, ac1_9);
        A1.Add(9, ac1_10);
        A1.Add(10, ac1_11);
        //存储audio第一个的选项的录音
        A2.Add(0, ac2_1);
        A2.Add(1, ac2_2);
        A2.Add(2, ac2_3);
        A2.Add(3, ac2_4);
        A2.Add(4, ac2_5);
        A2.Add(5, ac2_6);
        A2.Add(6, ac2_7);
        A2.Add(7, ac2_8);
        A2.Add(8, ac2_9);
        A2.Add(9, ac2_10);
        A2.Add(10, ac2_11);
        //存取所有Lipdata到键值对上
        LipData.Add(0, ls1);
        LipData.Add(1, ls2);
        LipData.Add(2, ls3);
        LipData.Add(3, ls4);
        LipData.Add(4, ls5);
        LipData.Add(5, ls6);
        LipData.Add(6, ls7);
        LipData.Add(7, ls8);
        LipData.Add(8, ls9);
        LipData.Add(9, ls10);
        //存取所有Animation到键值对上
    }


}
