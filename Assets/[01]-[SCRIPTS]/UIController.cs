﻿using RogoDigital.Lipsync;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    private Text btnT1;
    private Text btnT2;

    [Header("play to the Number of the textes")]
    public int count_changeTextes=0;
    [Header("play to the Number of the son lipSnyc")]
    public int count_lipSnyc = 0;
    [Header("play to the Number of the son docter")]
    public int count_docteurSon = 0;
    [Header("play to the Number of the animation")]
    public int count_animation = 0;

    [Header("string Denise")]
    public string Denise = "Denise";
    public GameObject denise;
    public AudioSource sourceConclu;
    Animator animDenise;


    private ValueTest valTest;
    private LipSync lipPlayer;
    private AudioSource audiosources;
    private AudioClip audioclip;

    private GameObject btn1;
    private GameObject btn2;
    public GameObject canvas;
    public GameObject player;
    public GameObject oldperson;
    [Header("son delay per second")][Range(0,10)]
    public float delayToSon=1.0f;
    [Header("Textes delay per second")][Range(0, 10)]
    public float delayToTextes=0.5f;
    [Header("Change la scenes delay per second")][Range(0, 30)]
    public float delayToChangeScene=1.0f;


    [Header("debut de scene")]
    public string debut;
    [Header("experience01")]
    public string ex01;
    [Header("experience02")]
    public string ex02;
    // Use this for initialization
    private void Awake()
    {
        //denise animtor 组件
        denise = GameObject.FindGameObjectWithTag(Denise);
        animDenise = denise.GetComponent<Animator>();


        valTest = gameObject.GetComponent<ValueTest>();
        btn1 = GameObject.FindGameObjectWithTag("Btn1");
        btn2 = GameObject.FindGameObjectWithTag("Btn2");
        btnT1 = btn1.GetComponentInChildren<Text>();
        btnT2 = btn2.GetComponentInChildren<Text>();
        //获取canvasGroup组件
        //canvas = GameObject.FindGameObjectWithTag("Canvas");
        canvasGroup = canvas.GetComponent<CanvasGroup>();
        //得到人物身上的组件LipSync
        //LipSync LipSyncPlayer = player.GetComponent<LipSync>();
        lipPlayer = oldperson.GetComponent<LipSync>();
        audiosources = player.GetComponent<AudioSource>();
        audioclip = player.GetComponent<AudioClip>();
    }
    void Start () {
        btnT1.text = valTest.Q1[count_changeTextes];
        btnT2.text = valTest.Q2[count_changeTextes];
        canvasGroup.alpha = 1;
    }
	
	// Update is called once per frame
	void Update () {

        if (count_docteurSon > 10)
        {
            count_docteurSon = -1;
            count_lipSnyc = -1;
            Debug.Log("derinier le son a prensent");
            //audiosources.Stop();
            //lipPlayer.Stop(true);
            //OpenCloseBtn(false);
            StartCoroutine(WaitforPlayDerinierSon());
            if (sourceConclu.isPlaying != true)
            {
                count_docteurSon = -1;
                count_lipSnyc = -1;
                StartCoroutine(WaitForChangeScene());
            }
        }
    }

    //----------------------------------- 第二个选项点击-----------------------------------
    public void Btn_click_1_choice()
    {
        Debug.Log("1 choice");
        ChangeTextes();
        OpenCloseBtn(true);//出现textes
        PlayerDocteur(valTest.A1);

    }
    //----------------------------------- 第二个选项点击-----------------------------------
    public void Btn_click_2_choice()
    {
        Debug.Log("2 choice");
        ChangeTextes();
        OpenCloseBtn(true);
        PlayerDocteur(valTest.A2);
    }

    //----------------------------------- 延迟执行函数 - 机器播放声音 -----------------------------------
    IEnumerator WaitforPlay()
    {
        //等待秒数
        yield return new WaitForSeconds(audiosources.clip.length+ delayToSon);
        //执行播放
        Debug.Log("机器播放+" + count_lipSnyc);
        lipPlayer.Play(valTest.LipData[count_lipSnyc++]);
        //lancer animation quand old person qui parler
        Debug.Log("lancer animation ——————————————————————————————————");
        PlayAnimtion();
        StartCoroutine(WaitforTexteOpen());
    }
    IEnumerator WaitforPlayDerinierSon()
    {
        //等待秒数
        yield return new WaitForSeconds(audiosources.clip.length + delayToSon);
        sourceConclu.Play();

    }
    //----------------------------------- 延迟执行函数 - 展开文字延迟 -----------------------------------
    IEnumerator WaitforTexteOpen(){
        yield return new WaitForSeconds(lipPlayer.defaultClip.length+ delayToTextes);
        OpenCloseBtn(true);
    }
    IEnumerator WaitForChangeScene()
    {
        yield return new WaitForSeconds(lipPlayer.defaultClip.length + 8);
        SceneManager.LoadScene(debut);
    }

    //----------------------------------- 改变文字 -----------------------------------
    public void ChangeTextes(){
        //Textes值的索引++
        if (count_changeTextes >= 10){
            count_changeTextes = 0;
        }else{
            count_changeTextes++;
        }
        //改变Textes
        btnT1.text = valTest.Q1[count_changeTextes];
        btnT2.text = valTest.Q2[count_changeTextes];
    }

    // ----------------------------------- 展开界面 ---------------------------------
    public void OpenCloseBtn(bool iSwitch)
    {
        if (iSwitch == true){
            //展开
            canvasGroup.alpha =1;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
            iSwitch = false;
        }else if (iSwitch == false) {
            //隐藏
            canvasGroup.alpha =0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            iSwitch = true;
        }
    }

    //----------------------------------- 播放player的声音 ---------------------------------
    public void PlayerDocteur(Dictionary<int, AudioClip> keys)
    {
        if (audiosources.isPlaying != true && count_docteurSon <= 10)
        {
            audiosources.clip = keys[count_docteurSon++];
            Debug.Log(count_docteurSon);
            audiosources.Play();
            OpenCloseBtn(false);
            //播放机器人的声音老年人的声音
            StartCoroutine(WaitforPlay());
        }
    }
    //播放动画Denise
    public void PlayAnimtion()
    {
        animDenise.SetTrigger("EmotionDenise");
    }



}
//foreach (KeyValuePair<int,string> kv in q1.Q1)
//{
//   Debug.Log(kv.Key+":"+kv.Value);
//}
//Debug.Log("------------------------");
//foreach (KeyValuePair<int, string> kv in q2.Q2)
//{
//   Debug.Log(kv.Key + ":" + kv.Value);
//}
//关闭textes选项

//重复
//if (lipPlayer.IsPlaying == false)
//{
//    Debug.Log("停了");
//    Debug.Log(i);
//    lipPlayer.Play(valTest.LipData[i++]);
//    OpenCloseBtn(true);
//}

//ctrl+k,Ctrl+c
//ctrl+K,Ctrl+u

//展开界面 
//canvasGroup.alpha +=t*iSpeed;
//获取时间
//t = Time.deltaTime;


//出现textes
//播放textes所对应的声音
//if (audiosources.isPlaying != true)
//{
//    audiosources.clip = valTest.A2[count_docteurSon++];
//    audiosources.Play();
//    OpenCloseBtn(false);
//    //播放机器人的声音老年人的声音
//    StartCoroutine(WaitforPlay());
//}


//播放textes所对应的声音
//if (audiosources.isPlaying != true)
//{
//    audiosources.clip = valTest.A1[count_docteurSon++];
//    audiosources.Play();
//    OpenCloseBtn(false);
//    //播放机器人的声音老年人的声音
//    StartCoroutine(WaitforPlay());
//} 
//----------------------------------- 结束 Scene ---------------------------------
//public void changeSecne()
//{
//    Debug.Log("ChangeScene");
//}